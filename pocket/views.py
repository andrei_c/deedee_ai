################################################################
#  de implementat list view la toate si update view
from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import UpdateView, CreateView, ListView, TemplateView, DeleteView
from pocket.form import IncomeCreateForm, MakePaymentCreateForm, BudgetCreateForm, ExpenseCreateForm
from pocket.models import UserPocket, UserPayment, Income, Budget, Expense


class ExpenseCreateView(LoginRequiredMixin, CreateView):
    template_name = 'pocket/add_view/expense_add.html'
    model = Expense
    form_class = ExpenseCreateForm
    success_url = reverse_lazy('wallet_main_page')

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        expenses = Expense.objects.all()
        context['expenses'] = expenses
        context['expense_total'] = expense_total(expenses)
        return context


class WalletTemplateView(LoginRequiredMixin, TemplateView):
    template_name = 'pocket/pocket_main_page.html'


def expense_total(expenses):
    expenses_total_var = sum([expense.value for expense in expenses])
    return expenses_total_var


class WalletListView(LoginRequiredMixin, ListView):
    template_name = 'pocket/pocket_list_view.html'
    model = UserPocket
    context_object_name = "all_from_pocket"

    def get_context_data(self, **kwargs):
        context = {}
        money_spent = Expense.objects.all()
        money_from_incomes = Income.objects.all()

        result = sum([income.value for income in money_from_incomes])
        result = str(result)
        context['monthly_income'] = result

        total_income = 0
        budget = Budget.objects.all()
        other_incomes = sum([bud.current_money for bud in budget])

        for income in money_from_incomes:
            total_income += income.value * no_months(income.date)
        aux = total_income
        total_income = str(total_income)
        context['money_made'] = total_income

        total_taxes = sum([expense.value for expense in money_spent])
        total_taxes = str(total_taxes)
        context['monthly_taxes'] = total_taxes

        payments = UserPayment.objects.all()
        total_payments_made = sum([payment.price for payment in payments])
        aux2 = total_payments_made
        total_payments_made = str(total_payments_made)
        context['total_payments'] = total_payments_made

        total_taxes_paid = 0
        for expense in money_spent:
            total_taxes_paid += expense.value * no_months(expense.date)
        saved_money_value = aux - total_taxes_paid - aux2 + other_incomes
        saved_money = str(aux - total_taxes_paid - aux2 + other_incomes)
        total_taxes_paid = str(total_taxes_paid)
        context['taxes_paid'] = total_taxes_paid
        context['other_incomes'] = str(other_incomes)



        context['money_saved'] = saved_money

        context['budget'] = Budget.objects.all()
        budget_dict = {}
        budget = Budget.objects.all()
        for value in budget:
            name = value.name
            result =(saved_money_value * value.percentage)/100
            context[name] = str(result)

        context['budgett'] = budget_dict

        return context


def no_months(income_date):
    end_date = datetime.today()
    start_date = income_date

    num_months = (end_date.year - start_date.year) * 12 + (end_date.month - start_date.month)
    return num_months-1


#####################################################


class BudgetCreateView(LoginRequiredMixin, CreateView):
    template_name = 'pocket/add_view/budget_add.html'
    model = Budget
    form_class = BudgetCreateForm
    context_object_name = "all_budgets"
    success_url = reverse_lazy('wallet_main_page')


class IncomeCreateView(LoginRequiredMixin, CreateView):
    template_name = 'pocket/add_view/income_add.html'
    model = Income
    form_class = IncomeCreateForm
    context_object_name = 'all_incomes'
    success_url = reverse_lazy('wallet_main_page')

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        income = Income.objects.all()
        # print(len(tasks))
        context['income_data'] = income
        return context


####################################################
# list view


class IncomeListView(LoginRequiredMixin, ListView):
    template_name = 'pocket/list_view/income_list_view.html'
    model = Income
    context_object_name = "all_incomes"


class BudgetListView(LoginRequiredMixin, ListView):
    template_name = 'pocket/list_view/budget_list_view.html'
    model = Budget
    context_object_name = "all_budgets"

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data()
    #     budget = Budget.objects.all()
    #     context['expenses'] = budget
    #     context['expense_total'] = expense_total(budget)
    #     return context


class ExpenseListView(LoginRequiredMixin, ListView):
    template_name = 'pocket/list_view/expense_list_view.html'
    model = Expense
    context_object_name = "all_expenses"


###############################################
# update view

class IncomeUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "pocket/update_view/income_update.html"
    model = Income
    fields = "__all__"
    success_url = reverse_lazy("income_list_view")


class BudgetUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "pocket/update_view/budget_update.html"
    model = Budget
    fields = "__all__"
    success_url = reverse_lazy("budget_list_view")


class ExpenseUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "pocket/update_view/expense_update.html"
    model = Expense
    fields = "__all__"
    success_url = reverse_lazy("expense_list_view")


class PaymentUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "pocket/update_view/payment_update.html"
    model = UserPayment
    fields = "__all__"
    success_url = reverse_lazy("payment_list_view")


##########################################################
# delete view


class IncomeDeleteView(LoginRequiredMixin, DeleteView):
    template_name = "pocket/delete_view/income_delete.html"
    model = Income
    success_url = reverse_lazy("wallet_main_page")


class ExpenseDeleteView(LoginRequiredMixin, DeleteView):
    template_name = "pocket/delete_view/expense_delete.html"
    model = Expense
    success_url = reverse_lazy("wallet_main_page")


class BudgetDeleteView(LoginRequiredMixin, DeleteView):
    template_name = "pocket/delete_view/budget_delete.html"
    model = Budget
    success_url = reverse_lazy("wallet_main_page")


class PaymentDeleteView(LoginRequiredMixin, DeleteView):
    template_name = "pocket/delete_view/income_delete.html"
    model = UserPayment
    success_url = reverse_lazy("wallet_main_page")


########################################################
# payments


class PaymentCreateView(LoginRequiredMixin, CreateView):
    template_name = 'pocket/add_view/payment_add.html'
    model = UserPayment
    form_class = MakePaymentCreateForm
    context_object_name = "all_payments"
    success_url = reverse_lazy('wallet_main_page')


class PaymentListView(LoginRequiredMixin, ListView):
    template_name = 'pocket/list_view/payment_list_view.html'
    model = UserPayment
    context_object_name = "all_payments"
