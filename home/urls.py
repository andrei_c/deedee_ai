from django.urls import path
from home.views import HomeTemplateView, chat_view, ContactTemplateView, IntroductionTemplateView

urlpatterns = [
    path('home/', HomeTemplateView.as_view(), name='home_page'),
    path('chat/', chat_view, name='chatbot'),
    path('contact/', ContactTemplateView.as_view(), name='contact'),
    path('', IntroductionTemplateView.as_view(), name='introd'),
    ]

