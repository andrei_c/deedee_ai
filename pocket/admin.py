from django.contrib import admin
from pocket.models import *

admin.site.register(Budget)
admin.site.register(Income)
admin.site.register(UserPayment)
admin.site.register(Expense)


