# Generated by Django 3.2.2 on 2021-05-26 15:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('users', '0002_extenduser_city'),
    ]

    operations = [
        migrations.CreateModel(
            name='TaskCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('task_name', models.CharField(max_length=50)),
                ('task_date', models.DateField()),
                ('task_information', models.CharField(max_length=50)),
                ('task_deadline', models.DateField()),
                ('task_status', models.IntegerField(default=0)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='task.taskcategory')),
                ('extend_user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='users.extenduser')),
            ],
        ),
    ]
