from django.db import models

from users.models import ExtendUser


class Income(models.Model):
    name = models.CharField(max_length=50)
    value = models.DecimalField(decimal_places=2, max_digits=10)
    date = models.DateField(null=False)


class Expense(models.Model):
    name = models.CharField(max_length=50)
    value = models.DecimalField(decimal_places=2, max_digits=10)
    date = models.DateField(null=False)


class Budget(models.Model):
    name = models.CharField(max_length=50)
    percentage = models.IntegerField(default=0)
    current_money = models.DecimalField(decimal_places=2, max_digits=10)
    extend_user = models.ForeignKey(ExtendUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class UserPayment(models.Model):

    product = models.CharField(max_length=50)
    price = models.DecimalField(decimal_places=2, max_digits=10)
    # budget_name = models.CharField(max_length=50)
    budget = models.ForeignKey(Budget, on_delete=models.DO_NOTHING)


class UserPocket(models.Model):
    incomes = models.ManyToManyField(Income)
    expenses = models.ManyToManyField(Expense)
    budgets = models.ManyToManyField(Budget)  # tip buget
    # category = models.ForeignKey(Category, on_delete=models.CASCADE)
    payment = models.ManyToManyField(UserPayment)


class PocketHistory(models.Model):
    incomes = models.DecimalField(decimal_places=2, max_digits=10)
    other_incomes = models.DecimalField(decimal_places=2, max_digits=10)
    expenses = models.DecimalField(decimal_places=2, max_digits=10)
    other_expenses = models.DecimalField(decimal_places=2, max_digits=10)