from django.contrib.auth.models import User
from django.db import models


class ExtendUser(User):
    phone = models.CharField(max_length=30)
    age = models.CharField(max_length=150)
    gender = models.CharField(max_length=150)
    date_of_birth = models.DateField(null=False)
    city = models.CharField(max_length=150)
