from django.contrib.auth.forms import UserCreationForm
from django.forms import TextInput, DateInput, NumberInput, EmailInput

from users.models import ExtendUser


class UserCreateForm(UserCreationForm):
    class Meta:
        model = ExtendUser
        fields = ['first_name', 'last_name', 'username', 'email','city','phone', 'age', 'date_of_birth', 'gender']
        widgets = {
            'first_name': TextInput(attrs={
                'placeholder': 'Please insert first name.',
                'class': 'input is-primary'
            }),
            'last_name': TextInput(attrs={
                'placeholder': 'Please insert last name.',
                'class': 'input is-primary'
            }),
            'username': TextInput(attrs={
                'placeholder': 'Please insert username.',
                'class': 'input is-primary'
            }),
            'email': EmailInput(attrs={
                'placeholder': 'Please insert email.',
                'class': 'input is-primary'

            }),
                'city': TextInput(attrs={
                'placeholder': 'Please insert your city.',
                'class': 'input is-primary'
            }),
            'phone': TextInput(attrs={
                'placeholder': 'Please insert phone number.',
                'class': 'input is-primary'
            }),
            'age': NumberInput(attrs={
                'placeholder': 'Please insert age.',
                'class': 'input is-primary'
            }),

            'date_of_birth': DateInput(attrs={
                'placeholder': '(yy-mm-dd)',
                'class': 'input is-primary'
            }),
            'gender': TextInput(attrs={
                'placeholder': 'Please insert gender.',
                'class': 'input is-primary'
            }),
        }

    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs['class'] = 'input is-primary'
        self.fields['password1'].widget.attrs['placeholder'] = 'Please insert password'
        self.fields['password2'].widget.attrs['class'] = 'input is-primary'
        self.fields['password2'].widget.attrs['placeholder'] = 'Please insert password confirmation'
