from django.urls import path
from users import views
from users.views import UserUpdateView, UserListView

urlpatterns = [
    path('users_create/', views.sign_up, name='users_create'),
    path('user_edit/<int:pk>/', UserUpdateView.as_view(), name='user_edit'),
    path('account/', UserListView.as_view(), name='account'),
]
