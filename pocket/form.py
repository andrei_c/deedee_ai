from django.forms import TextInput
from pocket.models import UserPayment, Income, Budget, Expense
from django import forms


class BudgetCreateForm(forms.ModelForm):
    class Meta:
        model = Budget
        fields = "__all__"
        widgets = {

            'name': TextInput(attrs={
                'placeholder': 'Insert budget name. ',
                'class': 'form-control'
            }),
            'percentage': TextInput(attrs={
                'placeholder': 'Percentage of your left income. ',
                'class': 'form-control'
            }),
            'current_money ': TextInput(attrs={
                'placeholder': "Your actual money.(fill this only once or only when you gain extra money whitch didn't come from your monthly income) ",
                'class': 'form-control',
                'required': False
            }),
        }


class ExpenseCreateForm(forms.ModelForm):
    class Meta:
        model = Expense
        fields = "__all__"
        widgets = {
            'name': TextInput(attrs={
                'placeholder': 'Insert expense name. ',
                'class': 'form-control'
            }),
            'value': TextInput(attrs={
                'placeholder': 'Insert value. ',
                'class': 'form-control'
            }),
            'date': TextInput(attrs={
                'placeholder': '(year-month-day) ',
                'class': 'form-control'
            }),
        }


class IncomeCreateForm(forms.ModelForm):
    class Meta:
        model = Income
        fields = "__all__"
        widgets = {
            'name': TextInput(attrs={
                'placeholder': 'Insert income name. ',
                'class': 'form-control'
            }),
            'value': TextInput(attrs={
                'placeholder': 'Insert value. ',
                'class': 'form-control'
            }),
            'date': TextInput(attrs={
                'placeholder': '(year-month-day) ',
                'class': 'form-control'
            }),
        }


class MakePaymentCreateForm(forms.ModelForm):
    class Meta:
        model = UserPayment
        fields = "__all__"
        widgets = {

            'product': TextInput(attrs={
                'placeholder': 'Insert payment name. ',
                'class': 'form-control'
            }),
            'price': TextInput(attrs={
                'placeholder': 'Insert product price. ',
                'class': 'form-control'
            }),
            'Budget': TextInput(attrs={
                'placeholder': 'Budget must exist !',
                'class': 'form-control'
            }),
        }

