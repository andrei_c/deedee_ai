from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import UpdateView, ListView

from users.forms import UserCreateForm
from users.models import ExtendUser


class SignUpForm:
    pass


# @login_required
# @permission_required('auth.add_user', login_url="/login/")
def sign_up(request):
    if request.method == 'POST':
        form = UserCreateForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return render(request, 'users/signup.html', {'form': form})
    else:
        form = UserCreateForm
    return render(request, 'users/signup.html', {'form': form})


class UserUpdateView(LoginRequiredMixin, UpdateView):
    template_name = "users/user_info_edit.html"
    model = ExtendUser
    fields = ['first_name', 'last_name', 'email', 'phone', 'city', 'age', 'date_of_birth', 'gender']
    success_url = reverse_lazy("home_page")


class UserListView(LoginRequiredMixin, ListView):
    template_name = 'users/account_data.html'
    model = ExtendUser
    context_object_name = "account_data"

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        account_data = ExtendUser.objects.filter(username=self.request.user.username)
        context['account_data'] = account_data
        return context
